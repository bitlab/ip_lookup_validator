import urllib.request
import json
from settings import IP_api_key



def Lookup(ip):
    response = urllib.request.Request("http://v2.api.iphub.info/ip/{}".format(ip))
    response.add_header("X-Key", IP_api_key)

    try:
        response = json.loads(urllib.request.urlopen(response).read().decode())
    except:
        return False  # In the case of an error, pass all IP's to avoid blocking innocents

    return response  # Defaults to None if failed to get block value