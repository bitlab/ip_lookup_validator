# IP Lookup Validator

## Purpose

The goal of this project is to validate IP addresses from qualtrics
surveys. The reason that this goal is necessary is due to an increase in
the use of Virtual Private Servers to defeat the location checks in
popular services like MTurk. See <https://wapo.st/2GaUjhc> for more
information

## How it works

The python script uses an API service hosted by IP Info
<https://iphub.info/>. The script calls the API and receives a json
object in return with the following information

* Information returned in JSON
    * IP address
    * Country Code
    * Country Name
    * ASN
    * isp - Internet Service Provider
    * block - iphub.info's Values (0,1,2)

Block is the important variable here. According to the
<https://iphub.info/api> documentation, block 1 and 2 are
non-residential ips but the API site recommends only blocking block 1.
The takeaway is if you run this script and you get block 1 ips they are
using a proxy/hosting provider and should be considered as a potentially
remove from your survey.

## Running Script

To run this script you should be using Python 3.x and have some idea how
to run a python script from the command line. The script has two
arguments

* -f --filename - the file name that contains the IP Addresses column. The column
    title needs to be in the first row and titled "IPAddress"
* -s --sepfile - this parameter specifies if you want to place the IP Addresses in
    a seperate file  or not.
* -m --mturk- This parameter specifies if you are running an mturk sample. If so
    can add this so the worker id will be in the output. This makes 
    approving mTurkers way easier. 
 
To run use the following in terminal

```
python main.py -s -m -f "myfile.csv"
```
OR
```
python main.py --sepfile --mturk --filename "myfile.csv"
```


## TODOs

Current the -s parameter needs to be added in order to be run. I mean
shit i did this in a few hours. Possibly add changes for -q

