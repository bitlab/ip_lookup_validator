from argparse import Namespace

import IPHub
import csv
import pandas as pd
from pandas.io.json import json_normalize
import json
import argparse
import time
import os

def parse_arguments():
    parser = argparse.ArgumentParser(description='Check Source for IP addresses from a CSV file')

    parser.add_argument('-f', '--filename', action='store', help="Name of the file that conatins the IP addresses")
    parser.add_argument('-s', '--sepfile', action='store_true',
                        help="If you want to append the results to the a different csv")
    parser.add_argument('-m', '--mturk', action='store_true', 
                        help='Use this if you want mturk worker id in the output')

    params = parser.parse_args()
    print(params)
    return params


def main():

    info = parse_arguments()


    # df = pd.read_csv("Security_Stories_Batch1_ManipulationCheck.csv")
    #print(os.getcwd())
    df = pd.read_csv(info.filename)
    #df = pd.read_csv('/OneDrive/Development/PythonProjects/ip_lookup_valid/Testing/SS+Prestudy+3_201904cp29.csv', engine=python)
    ip_addresses = df[['IPAddress','worker']]

    
    # for i,j in df.iterrows():
    #     print(i, j)
    #     for ip in df['IPAddress']:
    #         print(ip)


    # Blank list for the Json results we get back from the API
    ipdictionary = pd.DataFrame()

    # Code for seperate CSV file
    if info.sepfile:
        print("Outputing results to a seperate csv")

        for ip, worker in zip(ip_addresses['IPAddress'], ip_addresses['worker']):
            json_ip = IPHub.Lookup(ip)
            if json_ip == False:
                print('Not a Valid IP')
            elif json_ip != False:
                json_ip = json_normalize(json_ip)
                print(json_ip)
                #Merge Amazon worker with IP df
                #json_ip = json_ip.append(worker, ignore_index=True)
                if (info.mturk):
                    json_ip['Mturker worker'] = worker
                
                #ipdictionary.append(json_ip)
                ipdictionary = ipdictionary.append(json_ip, ignore_index=True)

        # Add all ips json into one pandas dataframe
        totalIPS = pd.DataFrame.from_dict(ipdictionary)
        totalIPS.join(df)
        #totalIPS = json_normalize(totalIPS, 'ip', 'block')

        # Write to CSV
        totalIPS.to_csv('IP address ' + time.strftime("%Y%m%d-%H%M%S") + ".csv", sep=',')

    # Code for appending to CSV file
    #TODO: Finish this so it appends to the CSV

if __name__ == "__main__":
    #execute only if run as a script
    main()